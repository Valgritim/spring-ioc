package org.eclipse.main;

import org.eclipse.model.Personne;
import org.eclipse.model.Voiture;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Personne p = context.getBean("per", Personne.class);
        p.afficher();
        
        System.out.println("------------------------------------------------------------------------");
        
        Voiture voiture = context.getBean("voiture", Voiture.class);
        voiture.afficher();
    }
}
